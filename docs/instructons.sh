# Unit1
cd catkin_ws
source devel/setup.bash
rospack profile
roslaunch teb_navigation gmapping.launch

rviz

roslaunch husky_launch keyboard_teleop.launch

# Save map
roscd teb_navigation;
mkdir maps;
cd maps;
rosrun map_server map_saver -f my_map;


# Use AMCL with the saved map
cd catkin_ws
source devel/setup.bash
rospack profile
roslaunch teb_navigation amcl.launch